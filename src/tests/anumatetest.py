'''
Created on Dec 9, 2014

    ANumate Program - program to help manage multi-repository projects on *nix systems
    Copyright (C) 2014    Applied Numerics, LLC (Scott Johnson)
    contact: scott.johnson@apnum.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

@author: scottjohnson
'''
import unittest
import numpy as np

class AnumateTest(unittest.TestCase):

    def test_first(self):
        val=0
        self.assertEqual(val, 0, "Value is not 0: {0}".format(val));
